package com.hudz.model.fibonacciTask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OneExecutorFibonacci {
    public static void go() {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 10; i++) {
            System.out.print(" Task#" + i);
            executor.execute(() -> {
                new MyThread().run();
            });
        }
        executor.shutdown();
    }
}
