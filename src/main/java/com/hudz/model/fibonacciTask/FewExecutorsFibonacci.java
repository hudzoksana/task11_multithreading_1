package com.hudz.model.fibonacciTask;

import java.util.concurrent.*;

public class FewExecutorsFibonacci {

    ExecutorService executor1 = Executors.newSingleThreadExecutor();
    ExecutorService executor2 = Executors.newFixedThreadPool(5);
    ExecutorService executor3 = Executors.newCachedThreadPool();

    public void doTask() {
        Runnable task = () -> new MyThread().run();

        for (int i = 0; i < 3; i++) {
            executor1.submit(task);
            executor2.submit(task);
            executor3.submit(task);
        }
        executor1.shutdown();
        executor2.shutdown();
        executor3.shutdown();
    }
}

