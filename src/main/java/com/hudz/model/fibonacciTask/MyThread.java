package com.hudz.model.fibonacciTask;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class MyThread implements Runnable, Callable<Integer> {
    @Override
    public void run() {
        System.out.println("started " + LocalDateTime.now() + " " + Thread.currentThread().getName());
        List<FibonacciNumbers> fibonacciNumbersList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName());
            fibonacciNumbersList.add(i, new FibonacciNumbers(1000));
         //   System.out.println(fibonacciNumbersList);
        }
        System.out.println("finished " + LocalDateTime.now() + " " + Thread.currentThread().getName());
    }

    @Override
    public Integer call() throws Exception {
        System.out.println("started " + LocalDateTime.now() + " " + Thread.currentThread().getName());
        int sum = 0;
        FibonacciNumbers fibonacciNumbers = new FibonacciNumbers(10);
        for (Integer l: fibonacciNumbers.getFibonacciNumbers()) {
            sum += l;
        }
        System.out.println("finished " + LocalDateTime.now() + " " + Thread.currentThread().getName());
        return sum;
    }
}