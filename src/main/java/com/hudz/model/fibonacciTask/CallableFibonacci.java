package com.hudz.model.fibonacciTask;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class CallableFibonacci {

    public void doTask() throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        List<Callable<Integer>> callables = Arrays.asList(()->new MyThread().call(), ()->new MyThread().call(), ()-> new MyThread().call());
        executor.invokeAll(callables).stream().map(future -> {
            try {
                return future.get();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }).forEach(System.out::println);
        executor.shutdown();
    }
}
