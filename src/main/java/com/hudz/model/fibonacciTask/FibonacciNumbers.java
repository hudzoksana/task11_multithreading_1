package com.hudz.model.fibonacciTask;

import java.util.ArrayList;
import java.util.List;

public class FibonacciNumbers {
    private List<Integer> fibonacciNumbers = new ArrayList<>();

    FibonacciNumbers(int n) {
        setFibonacciNumbers(n);
    }

    public List<Integer> getFibonacciNumbers() {
        return fibonacciNumbers;
    }

    private void setFibonacciNumbers(int n) {
        fibonacciNumbers.add(1);
        fibonacciNumbers.add(1);
        for (int i = 2; i < n; i++) {
            fibonacciNumbers.add(i, fibonacciNumbers.get(i - 1) + fibonacciNumbers.get(i - 2));
        }
    }

    @Override
    public String toString() {
        return "FibonacciNumbers{" +
                "fibonacciNumbers=" + fibonacciNumbers +
                '}';
    }
}

