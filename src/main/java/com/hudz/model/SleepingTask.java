package com.hudz.model;

import java.time.LocalTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SleepingTask {

        public static void taskSleep() {
            int random = (int)(Math.random()*10);
            try {
                System.out.println(LocalTime.now());
                TimeUnit.SECONDS.sleep(random);
                System.out.println(LocalTime.now());
                System.out.println("Time I slept is " + random + ". My thread is " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

         public static void doTask() {
             ScheduledExecutorService pool = Executors.newScheduledThreadPool(3);
             //ScheduledExecutorService pool = Executors.newSingleThreadScheduledExecutor();
             for (int i = 1; i <= 10; i++) {
                 pool.schedule(SleepingTask::taskSleep, i, TimeUnit.SECONDS);
             }
             pool.shutdown();
             try {
                 while (!pool.isTerminated()) {
                     pool.awaitTermination(12, TimeUnit.SECONDS);
                 }
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }
         }
}
