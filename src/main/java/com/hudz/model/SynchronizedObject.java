package com.hudz.model;

public class SynchronizedObject {
    private static long A = 0;
    private static Object sync = new Object();
    private static Object sync1 = new Object();
    private static Object sync2 = new Object();
    private static Object sync3 = new Object();

    StringBuilder sb = new StringBuilder("");
    StringBuilder sb1 = new StringBuilder("");


    public void runMethodSameObject1(StringBuilder sb){
        synchronized (sync) {
            try {
                for (int i = 0; i < 3; i++) {
                    Thread.sleep(3000);
                    sb.append("1st Method Same Obj\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void runMethodSameObject2(StringBuilder sb){
        synchronized (sync) {
            try {
                for (int i = 0; i < 3; i++) {
                    Thread.sleep(1000);
                    sb.append("2nd Method Same Obj\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void runMethodSameObject3(StringBuilder sb){
        synchronized (sync) {
            try {
                for (int i = 0; i < 3; i++) {
                    Thread.sleep(1000);
                    sb.append("3rd Method Same Obj\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void runMethodDiffObj1(StringBuilder sb){
        synchronized (sync1) {
            try {
                for (int i = 0; i < 3; i++) {
                    Thread.sleep(1000);
                    sb.append("1st Method Diff Obj\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void runMethodDiffObj2(StringBuilder sb){
        synchronized (sync2) {
            try {
                for (int i = 0; i < 3; i++) {
                    Thread.sleep(1000);
                    sb.append("2nd Method Diff Obj\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void runMethodDiffObj3(StringBuilder sb){
        synchronized (sync3) {
            try {
                for (int i = 0; i < 3; i++) {
                    Thread.sleep(1000);
                    sb.append("3rd Method Diff Obj\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public String getResultWIthTheSameObject() {
        Thread t1 = new Thread(() -> runMethodSameObject1(sb));
        Thread t2 = new Thread(() -> runMethodSameObject2(sb));
        Thread t3 = new Thread(() -> runMethodSameObject3(sb));
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join(); t2.join(); t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return  sb.toString();
    }

    public String getResultWithDifferentObjects() {
        Thread t1 = new Thread(() -> runMethodDiffObj1(sb1));
        Thread t2 = new Thread(() -> runMethodDiffObj2(sb1));
        Thread t3 = new Thread(() -> runMethodDiffObj3(sb1));
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join(); t2.join(); t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return sb1.toString();
    }
}



