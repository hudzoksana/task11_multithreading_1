package com.hudz.model;

import java.time.LocalDateTime;

public class PingPong {
    private volatile static boolean pingPong = false;
    private static Object sync = new Object();

    public static boolean isPingPong() {
        return pingPong;
    }

    public static void setPingPong(boolean pingPong) {
        PingPong.pingPong = pingPong;
    }

    public void play(int count) {
        Thread player1 = new Thread(() -> {
            synchronized (sync) {
                for (int i = 0; i < count; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pingPong = true;
                    System.out.println("Ping");
                    sync.notify();
                }
            }
        });
        Thread player2 = new Thread(() -> {
            synchronized (sync) {
                for (int i = 0; i < count; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pingPong = false;
                    System.out.println("Pong");
                }
            }
        });

        System.out.println(LocalDateTime.now());
        player1.start();
        player2.start();
        try {
            player1.join();
            player2.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(LocalDateTime.now());
    }
}

