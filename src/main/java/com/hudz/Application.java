package com.hudz;

import com.hudz.view.ConsoleMainMenu;

public class Application {
    public static void main(String[] args) {
        new ConsoleMainMenu().show();
    }
}
