package com.hudz.view;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.ResourceBundle;

public class ConsoleMainMenu extends AbstractMenu {

  private void setMenu() {

    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));
    menu.put("7", bundle.getString("7"));

    menu.put("Q", bundle.getString("Q"));
  }

  public ConsoleMainMenu() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();

    methodsMenu.put("1", this::getResultPingPongResult);
    methodsMenu.put("2", this::getResultFibonacciOneExecutor);
    methodsMenu.put("3", this::getResultFibonacciFewExecutors);
    methodsMenu.put("4", this::getResultWithCallableFewTasks);
    methodsMenu.put("5", this::getResultRansomSleepTask);
    methodsMenu.put("6", this::getResultWithSyncronized);
//    methodsMenu.put("7", this::);
  }

    private void getResultWithSyncronized() {
        logger.trace("If will synchronize methods on the same object:");
        logger.trace(controller.getResultWIthTheSameObject());
        logger.trace("If will synchronize methods on the different object:");
        logger.trace(controller.getResultWithDifferentObjects());
    }

    private void getResultRansomSleepTask() {
    logger.trace("Task: Sleep random from 1 to 10 sec. Using ScheduledThreadPool.");
    controller.getResultRansomSleepTask();
  }

  private void getResultWithCallableFewTasks() {
    logger.trace("Using Callable getting sum of 10 first Fibonacci numbers:");
    controller.getResultWithCallableFewTasks();
  }

  private void getResultFibonacciFewExecutors() {
    logger.trace("Few executor - few tasks");
    controller.getResultFibonacciFewExecutors();

  }

  private void getResultFibonacciOneExecutor() {
      logger.trace("One executor - few tasks");
      controller.getResultFibonacciOneExecutor();
  }

  private void getResultPingPongResult() {
    logger.trace("Playing ping pong for 10 times:");
    controller.getResultPingPong(10);
  }


  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }
}
