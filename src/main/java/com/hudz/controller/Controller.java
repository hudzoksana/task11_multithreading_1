package com.hudz.controller;

public abstract class Controller {


    public abstract void getResultPingPong(int count);

    public abstract void getResultFibonacciOneExecutor();

    public abstract void getResultFibonacciFewExecutors();

    public abstract void getResultWithCallableFewTasks();

    public abstract void getResultRansomSleepTask();

    public abstract String getResultWIthTheSameObject();

    public abstract String getResultWithDifferentObjects();
}
