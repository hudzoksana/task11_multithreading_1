package com.hudz.controller;

import com.hudz.model.PingPong;
import com.hudz.model.SleepingTask;
import com.hudz.model.SynchronizedObject;
import com.hudz.model.fibonacciTask.CallableFibonacci;
import com.hudz.model.fibonacciTask.FewExecutorsFibonacci;
import com.hudz.model.fibonacciTask.OneExecutorFibonacci;

import java.util.concurrent.ExecutionException;

public class ControllerMultiTheding extends Controller {

    SynchronizedObject synchronizedObject = new SynchronizedObject();
    PingPong pingPong = new PingPong();
    @Override
    public void getResultPingPong(int count) {
         pingPong.play(count);
    }

    @Override
    public void getResultFibonacciOneExecutor() {
        OneExecutorFibonacci.go();
    }

    @Override
    public void getResultFibonacciFewExecutors() {
        FewExecutorsFibonacci f = new FewExecutorsFibonacci();
        f.doTask();
    }

    @Override
    public void getResultWithCallableFewTasks() {
        CallableFibonacci c = new CallableFibonacci();
        try {
            c.doTask();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getResultRansomSleepTask() {
        SleepingTask.doTask();
    }

    @Override
    public String getResultWIthTheSameObject() {
        return synchronizedObject.getResultWIthTheSameObject();
    }

    @Override
    public String getResultWithDifferentObjects() {
        return synchronizedObject.getResultWithDifferentObjects();
    }
}
