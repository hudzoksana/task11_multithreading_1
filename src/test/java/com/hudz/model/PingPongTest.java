package com.hudz.model;



import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;

class PingPongTest {

    private static PingPong pingPong;

    @BeforeAll
    public static void initTest() {
        pingPong = new PingPong();
    }
    @Test
    void play() {

        int count = 10;
        boolean testPing = false;
        for (int i = 0; i < count; i++) {
            testPing = !testPing;
        }
        pingPong.play(count);
        assertEquals(PingPong.isPingPong(), testPing);
    }
}